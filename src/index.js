// Imports \\
const KiwiClient = require("./Structures/KiwiClient.js")
const config = require("../config.json")

// Main \\
const client = new KiwiClient(config)
client.connect()