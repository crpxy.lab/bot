// Imports \\
const { MessageEmbed } = require('discord.js')

// Main \\
class Embed extends MessageEmbed {
	constructor() {
		super()
		this.setColor("#5D3FD3")
		this.setFooter("Kiwi Chan | Develop By Crpxy#2194")
		this.setTimestamp()
	}
}

// Export \\
module.exports = Embed