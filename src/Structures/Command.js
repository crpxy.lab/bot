// Class \\
class Command {
	constructor(client, name, opt = {}) {
		this.client = client
		this.name = name
		this.aliases = opt.aliases || []
		this.description = opt.description || null
		this.usage = opt.usage || null
		this.category = opt.category || "Others"
		this.cooldown = opt.cooldown || null
		this.perm = opt.perm || null
		this.dm = opt.dm || false
		this.owner = opt.owner || false
	}

	async run(message, args) {
		throw new Error(`[*] > Command ${this.name} doesn't have a run function!`)
	}
}

// Export \\
module.exports = Command
