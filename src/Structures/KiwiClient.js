// Imports \\
const { Client, Collection, Intents } = require("discord.js")
const path = require("path")
const Enmap = require("enmap")

const Utils = require(path.join(__dirname, "Utils.js"))
const Modules = require(path.join(__dirname, "..", "Modules", "index.js"))

// Main \\
class KiwiClient extends Client {
	constructor(opt = {}) {
		super({
			intents: [
				Intents.FLAGS.GUILDS,
				Intents.FLAGS.GUILD_MESSAGES,
				Intents.FLAGS.GUILD_MESSAGE_REACTIONS
			],
			ws: {
				properties: {
					$browser: "Discord iOS"
				}
			}
		})

		this.commands = new Collection()
		this.events = new Collection()
		this.aliases = new Collection()

		this.config = opt

		this.db = {}
		this.db.cooldowns = new Enmap({
			name: "cooldowns",
			dataDir: path.join(process.cwd(), "Database", "cooldowns")
		})

		this.db.giveaways = new Enmap({
			name: "giveaways",
			dataDir: path.join(process.cwd(), "Database", "giveaways")
		})

		this.db.tickets = new Enmap({
			name: "tickets",
			dataDir: path.join(process.cwd(), "Database", "tickets")
		})

		this.utils = new Utils(this)
		this.modules = Modules(this)
	}

	async connect(token = this.config.token) {
		await this.utils.loadEvents()
		await this.utils.loadCommands()
		super.login(token)
	}
}

// Export \\
module.exports = KiwiClient