// Imports \\
const { MessageEmbed } = require("discord.js")
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))

// Class \\
class Reboot extends Command {
	constructor(...args) {
		super(...args, {
			description: "Reboot the bot",
			category: "Owner",
			aliases: ["restart"],
			owner: true
		})
	}

	async run(message) {
		const embed = new MessageEmbed()
			.setTitle("Bot Control")
			.setColor("#5D3FD3")
			.setDescription("Restarting...")
			.setFooter(`Requested by ${message.author.username}`, message.author.displayAvatarURL({ dynamic: true }))
			.setTimestamp();

		await message.channel.send({ embeds: [embed] })
		process.exit(1)
	}
}

module.exports = Reboot