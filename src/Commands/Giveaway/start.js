// Imports \\
const path = require("path")
const ms = require("ms")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class Start extends Command {
	constructor(...args) {
		super(...args, {
			description: "Start a giveaway",
			category: "Giveaway",
			cooldown: 5,
			dm: false,
			usage: "<duration> <winners> <prize> [drop?]"
		})
	}

	async run(message, [GD, PREGW, PREGP, PREISDROP]) {
		const embed = new Embed()
			.setTitle("Giveaway");

		if (!message.member.permissions.has('MANAGE_MESSAGES') && !message.member.roles.cache.some((r) => r.name === "Giveaways Role")) {
            embed.setDescription(":boom: You need to have the \`MANAGE_MESSAGES\` permissions to start giveaways.")

            return message.reply({ embeds: [embed] })
        }

        if (!GD || isNaN(ms(GD))) {
        	embed.setDescription(":boom: Hm. you haven\'t provided a duration. Can you try again?")

            return message.reply({ embeds: [embed] })
        }

        if (!PREGW || isNaN(parseInt(PREGW)) || parseInt(PREGW) <= 0) {
        	embed.setDescription(":boom: Uh... you haven\'t provided the amount of winners.")

            return message.reply({ embeds: [embed] })
        }

        const GW = PREGW.toLowerCase().replace("w", "")

        if (!PREGP) {
        	embed.setDescription(":boom: Oh, it seems like you didn\'t give me a valid prize!")

            return message.reply({ embeds: [embed] })
        }

        const GP = PREGP.replace(`"`, "").replace(`"`, "")

        const IsDrop = PREISDROP ? true  : false

        await this.client.modules.giveaway.start(message.channel, {
        	duration: ms(GD),
        	prize: GP,
        	winnerCount: parseInt(GW),
        	isDrop: IsDrop,
        	hostedBy: message.author
        })

        embed.setDescription(`Giveaway started in ${message.channel}`)

        const msg = await message.reply({ embeds: [embed] })
        setTimeout(() => msg.delete(), 10000);
	}
}

// Export \\
module.exports = Start