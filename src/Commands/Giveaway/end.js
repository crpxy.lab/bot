// Imports \\
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class End extends Command {
	constructor(...args) {
		super(...args, {
			description: "End a giveaway",
			category: "Giveaway",
			cooldown: 5,
			dm: false,
			usage: "<giveaway id>"
		})
	}

	async run(message, [ID]) {
		const embed = new Embed()
			.setTitle("Giveaway");

		const giveaway = this.client.modules.giveaway.giveaways.find((g) => g.messageId === ID && g.guildId === message.guild.id)

		if (!message.member.permissions.has('MANAGE_MESSAGES') && !message.member.roles.cache.some((r) => r.name === "Giveaways Role")) {
            embed.setDescription(":boom: You need to have the \`MANAGE_MESSAGES\` permissions to start giveaways.")

            return message.reply({ embeds: [embed] })
        }

        if (!giveaway) {
        	embed.setDescription(":boom: OOF I can't find this giveaway.")

            return message.reply({ embeds: [embed] })
        }

        if (giveaway.ended) {
        	embed.setDescription(":boom: Look Like This giveaway is already ended.")

            return message.reply({ embeds: [embed] })
        }

        await this.client.modules.giveaway.end(giveaway.messageId)

        const tem = "https://discord.com/channels"
        embed.setDescription(`Ended This Giveaway\n${tem}/${giveaway.guildId}/${giveaway.channelId}`)

        const msg = await message.reply({ embeds: [embed] })
        setTimeout(() => msg.delete(), 10000);
	}
}

// Export \\
module.exports = End