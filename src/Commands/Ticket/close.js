// Imports \\
const { MessageEmbed } = require("discord.js")
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))

// Class \\
class Close extends Command {
	constructor(...args) {
		super(...args, {
			description: "Close a ticket",
			category: "Ticket",
			cooldown: 20,
			dm: false
		})
	}

	async run(message) {
		await this.client.modules.ticket.close(message)
	}
}

// Export \\
module.exports = Close