// Imports \\
const { MessageEmbed } = require("discord.js")
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))

// Class \\
class Reopen extends Command {
	constructor(...args) {
		super(...args, {
			description: "Reopen a ticket",
			category: "Ticket",
			cooldown: 20,
			dm: false
		})
	}

	async run(message) {
		await this.client.modules.ticket.reopen(message)
	}
}

// Export \\
module.exports = Reopen