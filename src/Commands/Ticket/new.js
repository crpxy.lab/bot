// Imports \\
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class New extends Command {
	constructor(...args) {
		super(...args, {
			description: "Create new ticket",
			category: "Ticket",
			cooldown: 20,
			dm: false
		})
	}

	async run(message) {
		const embed = new Embed()
			.setTitle("Ticket")

		const TicketCount = await this.client.db.tickets.get("ticketCount", message.author.id)
		const TicketConfig = this.client.config.ticket

		if (!TicketCount) {
			await this.client.db.tickets.set("ticketCount", 0, message.author.id)
		}

		if (TicketCount >= TicketConfig.limit) {
			embed.setDescription(`:boom: Look like you open ticket more than rate limit per user (${TicketConfig.limit} ticket)`)
		
			return message.reply({ embeds: [embed] })
		}

		const TicketChannel = await this.client.modules.ticket.new(message)

		await this.client.db.tickets.inc("ticketCount", message.author.id)
		embed.setDescription(`**👋 Hey ${message.author.username}, Here is your ticket ${TicketChannel}**`)
		return message.reply({ embeds: [embed] })
	}
}

// Export \\
module.exports = New