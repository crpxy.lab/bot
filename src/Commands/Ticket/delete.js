// Imports \\
const { MessageEmbed } = require("discord.js")
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))

// Class \\
class Delete extends Command {
	constructor(...args) {
		super(...args, {
			description: "Delete a ticket",
			category: "Ticket",
			cooldown: 20,
			dm: false
		})
	}

	async run(message) {
		await this.client.modules.ticket.delete(message)
	}
}

// Export \\
module.exports = Delete