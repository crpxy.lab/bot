// Imports \\
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class Help extends Command {
	constructor(...args) {
		super(...args, {
			description: "Show all the bot commands",
			category: "General",
			usage: "[command]",
			cooldown: 4,
			dm: true
		})
	}

	async run(message, [command]) {
		const embed = new Embed()
			.setTitle("Help Menu");

		if (command) {
			const cmd = this.client.commands.get(command) || this.client.commands.get(this.client.aliases.get(command))

			if (!cmd) {
				embed
					.setTitle("ERROR")
					.setDescription("That is not a valid command or alias");

				return message.reply({ embeds: [embed] })
			}

			embed
				.setDescription([
					`**❯ Name:** \`${cmd.name}\``,
					`**❯ Aliases:** \`${cmd.aliases.length ? cmd.aliases.map(i => `${i}`).join(" ") : "No Aliases"}\``,
					`**❯ Description:** \`${cmd.description ? cmd.description : "No Description"}\``,
					`**❯ Category:** \`${cmd.category}\``,
					`**❯ Usage:** \`!${cmd.name} ${cmd.usage ? cmd.usage : ""}\``,
					`**❯ Cooldown:** \`${cmd.cooldown ? cmd.cooldown : "0"}\``,
					`**❯ DM:** \`${cmd.dm}\``,
					`**❯ Owner:** \`${cmd.owner}\``
					].join("\n"));

			return message.reply({ embeds: [embed] })
		} else {
			embed
				.setDescription([
						`My prefix is: \`${this.client.config.prefix}\``,
						`Command Parameters: \`<>\` is request \`[]\` is optional`
					].join("\n"));

			let cates = this.client.utils.removeDup(this.client.commands.map(cmd => cmd.category));
			if (!this.client.utils.isOwners(message.author.id)) {
				cates = this.client.utils.removeDup(this.client.commands.filter(cmd => cmd.category !== "Owner").map(cmd => cmd.category));
			}

			for (const cate of cates) {
				embed.addField(`**${cate}**`, this.client.commands.filter(cmd => cmd.category ===  cate).map(cmd => `\`${cmd.name}\``).join(","))
			}

			return message.reply({ embeds: [embed] })
		}
	}
}

// Export \\
module.exports = Help