// Imports \\
const path = require("path")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class Ping extends Command {
	constructor(...args) {
		super(...args, {
			description: "Show the bot letency",
			category: "General",
			cooldown: 5,
			dm: true
		})
	}

	async run(message) {
		const embed = new Embed()
			.setTitle("Pong 🏓")
			.setDescription("Pinging...");

		const msg = await message.channel.send({ embeds: [embed] })
		const latency = msg.createdTimestamp - message.createdTimestamp
		const apiPing = Math.round(this.client.ws.ping)

		let latencyRate = "🟢 LOW"
		let apiPingRate = "🟢 LOW"

		if (latency > 2000) {
			latencyRate = "⚫ ALIVE?"
		} else if (latency > 1000) {
			latencyRate = "🔴 VERY HIGH"
		} else if (latency > 500) {
			latencyRate = "🟡 HIGH"
		}

		if (apiPing > 2000) { 
			apiPingRate = "⚫ ALIVE?"
		} else if (apiPing > 1000) {
			apiPingRate = "🔴 VERY HIGH"
		} else if (apiPing > 500) {
			apiPingRate = "🟡 HIGH"
		}

		embed.setDescription([
			`**Bot Latency:** \`[${latencyRate}] ${latency}ms\``,
			`**Api Latency:** \`[${apiPingRate}] ${apiPing}ms\``
			].join("\n"))

		msg.edit({ embeds: [embed] })
	}
}

// Export \\
module.exports = Ping