// Imports \\
const { version } = require("discord.js")
const path = require("path")
const os = require('os');
const ms = require('ms');
const humanizeDuration = require("humanize-duration")

const Command = require(path.join(__dirname, "..", "..", "Structures", "Command.js"))
const Embed = require(path.join(__dirname, "..", "..", "Structures", "Embed.js"))

// Class \\
class Info extends Command {
	constructor(...args) {
		super(...args, {
			description: "Show the bot info",
			category: "General",
			cooldown: 4,
			dm: true
		})
	}

	async run(message) {
		const uptime = humanizeDuration(this.client.uptime, { round: true })
		const core = os.cpus()[0];

		const embed = new Embed()
			.setTitle("BotInfo")
			.addField("General", [
				`**❯ Name:** \`${this.client.user.tag}\``,
				`**❯ Developer:** \`CrpxyLab#2194\``,
				`**❯ Uptime:** \`${uptime}\``,
				`**❯ Users:** \`${this.client.guilds.cache.reduce((a, b) => a + b.memberCount, 0).toLocaleString()}\``,
				`**❯ Channels:** \`${this.client.channels.cache.size.toLocaleString()}\``,
				`**❯ Creation Date:** \`${utc(this.client.user.createdTimestamp).format('Do MMMM YYYY HH:mm:ss')}\``,
				`**❯ Discord.js:** \`v${version}\``,
				`**❯ Node.js:** \`${process.version}\``
			].join("\n"))
			.addField('System', [
				`**❯ Platform:** \`${process.platform}\``,
				`**❯ Uptime:** \`${ms(os.uptime() * 1000, { long: true })}\``,
				`**❯ CPU:**`,
				`\u3000 **cores:** \`${os.cpus().length}\``,
				`\u3000 **Model:** \`${core.model}\``,
				`\u3000 **Speed:** \`${core.speed}MHz\``,
				`**❯ Memory:**`,
				`\u3000 **Total:** \`${this.client.utils.formatBytes(process.memoryUsage().heapTotal)}\``,
				`\u3000 **Used:** \`${this.client.utils.formatBytes(process.memoryUsage().heapUsed)}\``
			].join("\n"));

		return await message.channel.send({ embeds: [embed] })
	}
}

// Export \\
module.exports = Info