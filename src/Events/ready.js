// Imports \\
const path = require("path")

const Event = require(path.join(__dirname, "..", "Structures", "Event.js"))

// Class \\
class Ready extends Event {
	constructor(...args) {
		super(...args, {
			once: true
		})
	}

	async run() {
		let index = 1

		const statusList = [
			`[${this.client.config.prefix}help] KiwiraHub`,
			`[${this.client.config.prefix}help] Crpxy#2194`
		]

		await this.client.user.setActivity(statusList[index], { type: "WATCHING" })

		setInterval(async () => {
			if (index == 0) {
				index = 1
			} else if (index == 1) {
				index = 0
			}

			
			await this.client.user.setActivity(statusList[index], { type: "WATCHING" })
		}, 10000)

		console.log([
				`Logged in as ${this.client.user.tag}`,
				`Loaded ${this.client.commands.size} commands!`,
				`Loaded ${this.client.events.size} events!`
			].join("\n"))
	}
}

// Export \\
module.exports = Ready
