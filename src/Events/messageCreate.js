// Imports \\
const path = require("path")
const humanizeDuration = require("humanize-duration")

const Event = require(path.join(__dirname, "..", "Structures", "Event.js"))
const Embed = require(path.join(__dirname, "..", "Structures", "Embed.js"))

// Class \\
class Message extends Event {
	async run(message) {
		if (message.author.bot) return

		this.client.db.cooldowns.ensure(message.author.id, {})

		const mentionRegex = RegExp(`^<@!${this.client.user.id}>$`)
		const mentionRegexPrefix = RegExp(`^<@!${this.client.user.id}> `)

		const embed = new Embed()

		if (message.content.match(mentionRegex)) {
			embed
				.setTitle("Prefix")
				.setDescription(`My prefix is \`${this.client.config.prefix}\``);

			return message.reply({ embeds: [embed] })
		}

		const prefix = message.content.match(mentionRegexPrefix) ? message.content.match(mentionRegexPrefix)[0] : this.client.config.prefix
		const [cmd, ...args] = message.content.slice(prefix.length).trim().split(/ +/g)

		const command = this.client.commands.get(cmd.toLowerCase()) || this.client.commands.get(this.client.aliases.get(cmd.toLowerCase()))

		if (!command)
			return

		if (!message.guild && !command.dm) {
			embed
				.setTitle("DM")
				.setDescription(`You may only use that command in servers!`);

			return message.reply({ embeds: [embed] })
		}

		if (command.cooldown) {
			if (await this.client.modules.cooldown.hasCooldown(message.author.id, command.name)) {
				embed
					.setTitle("Cooldown")
					.setDescription(`You need to wait **${humanizeDuration(await this.client.modules.cooldown.getCooldown(message.author.id, command.name), { round: true })}** to use command again`);

				return message.reply({ embeds: [embed] })
			}
			await this.client.modules.cooldown.addCooldown(message.author.id, command.name, command.cooldown)
		}

		if (command.owner && !this.client.utils.isOwners(message.author.id))
			return

		try {
			command.run(message, args)
		} catch(err) {
			embed
				.setTitle("ERROR")
				.setDescription(`Error while run ${command.name} command\n\`${err}\``);
		
			return message.reply({ embeds: [embed] })
		}
	}
}

// Export \\
module.exports = Message
