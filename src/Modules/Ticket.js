const path = require("path")
const Embed = require(path.join(__dirname, "..", "Structures", "Embed.js"))

class Ticket {
	constructor(client) {
		this.client = client
		this.config = this.client.config.ticket

		this.client.db.tickets.ensure("allTicketCount", 0)
		this.client.db.tickets.ensure("ticketCount", {})
	}

	async new(message) {
		const TicketNumber = await this.client.db.tickets.get("allTicketCount") + 1
		const TicketName = `ticket-${TicketNumber}`
		const LogChannel = message.guild.channels.cache.get(this.config.log_channel)
		const TicketChannel = await message.guild.channels.create(TicketName, {
			parent: this.config.category,
			permissionOverwrites: [
				{
					allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
					id: message.author.id
				},
				{
					allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
					id: this.config.roles[0]
				},
				{
					allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
					id: this.config.roles[1]
				},
				{
					deny: ["VIEW_CHANNEL", "SEND_MESSAGES"],
					id: message.guild.id
				}
			],
			reason: "Created a new ticket"
		})

		await this.client.db.tickets.inc("allTicketCount")
		await this.client.db.tickets.set(TicketChannel.id, {
			category: this.config.category,
            ticket: TicketChannel.id,
            guild: message.guild.id,
            user: message.author.id,
            open: true,
            number: TicketNumber
		})

		const embed = new Embed()
			.setTitle("Ticket")
			.setDescription("Thank you for creating a ticket! Suport will be with you shortly. To close this ticket use \`!close\`");

		TicketChannel.send({
			embeds: [embed]
		})

		TicketChannel.send({
			content: `${message.guild.roles.cache.get(this.config.roles[0])} ${message.guild.roles.cache.get(this.config.roles[1])} Please Review This Ticket!`
		})

		embed
			.setDescription(`${message.author.tag} Created a new ticket!`)
			.addField("Ticket Name", TicketChannel.name, true)
			.addField("Ticket ID", TicketChannel.id, true)
			.addField("Ticket Created At", `<t:${Math.floor(TicketChannel.createdTimestamp / 1000)}:R>`)

		LogChannel.send({
			embeds: [embed]
		})

		return TicketChannel
	}

	async close(message) {
		const TicketData = await this.client.db.tickets.get(message.channel.id)
		const LogChannel = message.guild.channels.cache.get(this.config.log_channel)

		const embed = new Embed()
			.setTitle("Ticket");

		if (!TicketData || TicketData === {}) {
			embed.setDescription("OOF You need use this command in ticket!")

			return message.reply({ embeds: [embed] })
		}

		if (!TicketData["open"]) {
			embed.setDescription("Amm I think this ticket is now close!!!")

			return message.reply({ embeds: [embed] })
		}

		await message.channel.permissionOverwrites.set([
			{
				id: message.guild.id,
				deny: ["VIEW_CHANNEL", "SEND_MESSAGES"]
			}
		])
		await message.channel.setName(`close-ticket-${TicketData.number}`)
		await this.client.db.tickets.set(message.channel.id, false, "open")

		embed.setDescription("**🔒 Ticket has been closed**")
		message.reply({ embeds: [embed] })

		embed
			.setDescription(`${message.author.tag} Closed ${message.channel}!`)
			.addField("Ticket Name", message.channel.name, true)
			.addField("Ticket ID", message.channel.id, true)
			.addField("Ticket Created At", `<t:${Math.floor(message.channel.createdTimestamp / 1000)}:R>`)
			.addField("Ticket Closed At", `<t:${Math.floor(Date.now() / 1000)}:R>`, true)
		return LogChannel.send({
			embeds: [embed]
		})
	}

	async reopen(message) {
		const TicketData = await this.client.db.tickets.get(message.channel.id)
		const LogChannel = message.guild.channels.cache.get(this.config.log_channel)

		const embed = new Embed()
			.setTitle("Ticket");

		if (!TicketData || TicketData === {}) {
			embed.setDescription("OOF You need use this command in ticket!")

			return message.reply({ embeds: [embed] })
		}

		if (TicketData["open"]) {
			embed.setDescription("Amm I think this ticket is already open!!!")

			return message.reply({ embeds: [embed] })
		}

		await message.channel.permissionOverwrites.set([
			{
				allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
				id: TicketData.user
			},
			{
				allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
				id: this.config.roles[0]
			},
			{
				allow: ["VIEW_CHANNEL", "SEND_MESSAGES"],
				id: this.config.roles[1]
			},
			{
				deny: ["VIEW_CHANNEL", "SEND_MESSAGES"],
				id: message.guild.id
			}
		])
		await message.channel.setName(`ticket-${TicketData.number}`)
		await this.client.db.tickets.set(message.channel.id, true, "open")

		embed.setDescription("**✅ Ticket has been reopend**")
		message.reply({ embeds: [embed] })

		embed
			.setDescription(`${message.author.tag} Reopened ${message.channel}!`)
			.addField("Ticket Name", message.channel.name, true)
			.addField("Ticket ID", message.channel.id, true)
			.addField("Ticket Created At", `<t:${Math.floor(message.channel.createdTimestamp / 1000)}:R>`)
			.addField("Ticket Reopened At", `<t:${Math.floor(Date.now() / 1000)}:R>`, true)
		return LogChannel.send({
			embeds: [embed]
		})
	}

	async delete(message) {
		const TicketData = await this.client.db.tickets.get(message.channel.id)
		const LogChannel = message.guild.channels.cache.get(this.config.log_channel)

		const embed = new Embed()
			.setTitle("Ticket");

		if (!TicketData || TicketData === {}) {
			embed.setDescription("OOF You need use this command in ticket!")

			return message.reply({ embeds: [embed] })
		}

		if (TicketData["open"]) {
			embed.setDescription("You need to use this command in close ticket!!!")

			return message.reply({ embeds: [embed] })
		}

		embed
			.setDescription(`${message.author.tag} Delete Ticket ${TicketData["number"]}!`)
			.addField("Ticket Name", message.channel.name, true)
			.addField("Ticket ID", message.channel.id, true)
			.addField("Ticket Created At", `<t:${Math.floor(message.channel.createdTimestamp / 1000)}:R>`)
			.addField("Ticket Deleted At", `<t:${Math.floor(Date.now() / 1000)}:R>`, true)
			
		LogChannel.send({
			embeds: [embed]
		})

		await this.client.db.tickets.delete(message.channel.id)
		return message.channel.delete(`Ticket Delete By ${message.author.tag}`)
	}
}

// Export \\
module.exports = Ticket