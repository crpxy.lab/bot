const Cooldown = require("./Cooldown.js")
const Giveaway = require("./Giveaway.js")
const Ticket = require("./Ticket.js")

function load(client) {
	return {
		cooldown: new Cooldown(client),
		giveaway: new Giveaway(client),
		ticket: new Ticket(client)
	}
}

module.exports = load