// This Class Steal from FlaringPheonix
// Class \\
class Cooldown {
	constructor(client) {
		this.client = client
	}

	async addCooldown(userid, command, time) {
		const ctime = Date.now() + (time*1000)
		await this.client.db.cooldowns.set(userid, ctime, command)
	
		setTimeout(() => {
			this.resetCooldown(userid, command)
		}, ctime - Date.now())
	}

	async getCooldown(userid, command) {
		const cooldowns = await this.client.db.cooldowns.get(userid)

		if (!cooldowns[command])
			return 0

		return Date.now() - cooldowns[command]
	}

	async hasCooldown(userid, command) {
		const cooldowns = await this.client.db.cooldowns.get(userid)

		if (!cooldowns[command])
			return false

		return true
	}

	async resetCooldown(userid, command) {
		return await this.client.db.cooldowns.delete(userid, command)
	}

	async resetAllCooldown(userid) {
		return await this.client.db.cooldowns.set(userid, {})
	}
}

// Export \\
module.exports = Cooldown