const { GiveawaysManager } = require('discord-giveaways')

class Giveaway extends GiveawaysManager {
	constructor(client) {
		super(client, {
			default: {
				botsCanWin: false,
				embedColor: "#5D3FD3",
				reaction: "🎉",
				lastChance: {
					enabled: true,
					content: '⚠️ **LAST CHANCE TO ENTER !** ⚠️',
            		threshold: 5000,
            		embedColor: '#5D3FD3'
				}
			}
		});

		this.client = client
	}
	
	async getAllGiveaways() {
		const res = await this.client.db.giveaways.fetchEverything().array()
		return res
	}

	async saveGiveaway(messageId, data) {
		await this.client.db.giveaways.set(messageId, data)
		return true
	}

	async editGiveaway(messageId, data) {
		await this.client.db.giveaways.set(messageId, data)
		return true
	}

	async deleteGiveaway(messageId) {
		await this.client.db.giveaways.delete(messageId)
		return true
	}
}

// Export \\
module.exports = Giveaway